package main

import (
	"encoding/json"
	"log"
	"net/http"
	"time"
)

func main() {
	var h CebolaHandler

	s := &http.Server{
		Addr:           "127.0.0.1:8080",
		Handler:        h,
		ReadTimeout:    1 * time.Second,
		WriteTimeout:   1 * time.Second,
	}

	log.Fatal(s.ListenAndServe())
}

type CebolaHandler int

func (CebolaHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	t1 := Team{Id: "Bia-Gui", Name: "BiGui"}
	t2 := Team{Id: "Dante-Igor", Name: "DanGor"}
	t3 := Team{Id: "Alex-Luiz", Name: "Aliz"}
	t4 := Team{Id: "Tony-Pedro", Name: "Todro"}
	t5 := Team{Id: "Hen-Claudio", Name: "Henio"}
	t6 := Team{Id: "Migue-Gabi", Name: "Migabi"}
	t7 := Team{Id: "CaioFacil", Name: "EasyCaio"}
	t8 := Team{Id: "CaioAdv", Name: "CaioHard"}

	teams := []Team{t1, t2, t3, t4, t5, t6, t7, t8}
	playoff := &Playoff{}
	playoff.CreateMatches(teams)

	b, _ := json.MarshalIndent(playoff, "", "\t")
	w.Write(b)
}

type Playoff struct {
	Type    string  `json:"type"`
	Matches []Match `json:"matches"`
}

func (p *Playoff) CreateMatches(teams []Team) {
	var matches []Match

	for i := 0; i < len(teams); i = i + 2 {
		m := Match{TeamA: teams[i], TeamB: teams[i+1]}
		matches = append(matches, m)
	}

	p.Type = "playoff"
	p.Matches = matches
}

type Match struct {
	TeamA Team `json:"teamA"`
	TeamB Team `json:"teamB"`
}

type Team struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}
